package com.zebrunner.iam.domain.search;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum SortOrder {

    ASC(Sort.Direction.ASC),
    DESC(Sort.Direction.DESC);

    private final Sort.Direction direction;

    public Sort.Direction toDirection() {
        return direction;
    }

}
