package com.zebrunner.iam.persistance;

import com.zebrunner.iam.domain.entity.LdapConfiguration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LdapConfigurationRepository extends JpaRepository<LdapConfiguration, Integer> {
}
