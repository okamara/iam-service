package com.zebrunner.iam.service.exception;

import com.zebrunner.iam.exception.ErrorDetail;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum BusinessValidationError implements ErrorDetail {

    GROUP_NAME_ALREADY_EXISTS(1300),
    GROUP_HAS_ASSIGNED_USERS(1301),
    USERNAME_ALREADY_EXISTS(1302),
    USER_EMAIL_ALREADY_EXISTS(1303),
    USERNAME_OR_EMAIL_ALREADY_EXISTS(1304),
    USER_NOT_FOUND_IN_LDAP(1305),
    WRONG_OLD_PASSWORD(1306),
    USER_HAS_ADDED_TO_GROUP(1307),
    USER_HAS_NOT_ADDED_TO_GROUP(1308),

    VALUE_NOT_PROVIDED(1309),
    PATCH_REQUEST_OPERATION_NOT_SUPPORTED_FOR_PATH(1310),
    PATCH_REQUEST_VALUE_HAS_WRONG_TYPE(1311);

    private final Integer code;

}
