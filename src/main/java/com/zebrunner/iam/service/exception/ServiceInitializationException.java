package com.zebrunner.iam.service.exception;

public class ServiceInitializationException extends RuntimeException {

    public ServiceInitializationException(String message) {
        super(message);
    }

}
