package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Permission;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.persistance.PermissionRepository;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import com.zebrunner.iam.service.util.StreamUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PermissionServiceImpl implements PermissionService {

    private static final String WILDCARD_SUFFIX = "*";

    private final PermissionRepository permissionRepository;

    @Override
    public List<Permission> getAll() {
        return permissionRepository.findAll();
    }

    @Override
    public Set<Permission> getConciseSetByNames(Collection<String> names) {
        List<Permission> foundPermissions = permissionRepository.findByNameIn(names);

        Set<String> foundNames = StreamUtils.mapToSet(foundPermissions, Permission::getName);
        List<String> notFoundNames = StreamUtils.filterToList(names, Predicate.not(foundNames::contains));
        if (!notFoundNames.isEmpty()) {
            throw new EntityNotFoundException(EntityNotFoundError.PERMISSIONS_NOT_FOUND_BY_NAMES, notFoundNames);
        }

        removeSubPermissions(foundPermissions);
        return new HashSet<>(foundPermissions);
    }

    private void removeSubPermissions(List<Permission> permissions) {
        // after the sorting, wildcards will be before then their's sub-permissions
        permissions.sort(Comparator.comparing(permission -> permission.getName().length()));

        Iterator<Permission> permissionIterator = permissions.iterator();
        String previousWildcard = null;

        while (permissionIterator.hasNext()) {
            String permissionName = permissionIterator.next().getName();
            if (previousWildcard != null && permissionName.startsWith(previousWildcard)) {
                permissionIterator.remove();
            } else if (permissionName.endsWith(WILDCARD_SUFFIX)) {
                previousWildcard = permissionName.replace(WILDCARD_SUFFIX, "");
            }
        }
    }

    @Override
    public Set<String> getSuperset(Collection<Group> groups, Collection<Permission> permissions) {
        Set<String> permissionsSuperset = groups.stream()
                                                .map(Group::getPermissions)
                                                .flatMap(Collection::stream)
                                                .map(Permission::getName)
                                                .collect(Collectors.toCollection(TreeSet::new));

        Set<String> permissionNames = StreamUtils.mapToSet(permissions, Permission::getName);
        permissionsSuperset.addAll(permissionNames);

        String searchString = permissionsSuperset.stream()
                                                 .filter(permissionName -> permissionName.endsWith(WILDCARD_SUFFIX))
                                                 .map(permissionName -> permissionName.replace(WILDCARD_SUFFIX, "%"))
                                                 .collect(Collectors.joining("|"));
        List<Permission> resolvedPermissions = permissionRepository.findByNameSimilarTo(searchString);
        resolvedPermissions.forEach(permission -> permissionsSuperset.add(permission.getName()));

        return permissionsSuperset;
    }
}
