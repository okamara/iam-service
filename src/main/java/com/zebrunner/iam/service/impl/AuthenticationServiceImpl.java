package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.entity.Source;
import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.token.AuthTokenContent;
import com.zebrunner.iam.domain.token.AuthenticationData;
import com.zebrunner.iam.domain.token.RefreshTokenContent;
import com.zebrunner.iam.exception.AuthException;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.exception.ForbiddenOperationException;
import com.zebrunner.iam.service.AuthenticationService;
import com.zebrunner.iam.service.JwtService;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.config.TokenProperties;
import com.zebrunner.iam.service.exception.AuthenticationError;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import com.zebrunner.iam.service.exception.ForbiddenOperationError;
import com.zebrunner.iam.web.security.AuthenticatedUser;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Objects;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final JwtService jwtService;
    private final UserService userService;
    private final TokenProperties tokenProperties;
    private final PermissionService permissionService;
    private final AuthenticationManager authenticationManager;
    private final AuthenticationManager ldapAuthenticationManager;

    @Override
    @Transactional
    public AuthenticationData authenticate(String username, String password) {
        User user = userService.getByUsernameOrEmail(username)
                               .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_USERNAME, username));

        Set<String> permissionsSuperset = Set.of();
        try {
            Authentication authenticationToken = new UsernamePasswordAuthenticationToken(user.getUsername(), password);

            AuthenticationManager authenticationManager = getAuthenticationManager(user);
            Authentication authentication = authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            if (authentication.getPrincipal() instanceof AuthenticatedUser) {
                AuthenticatedUser authenticatedUser = (AuthenticatedUser) authentication.getPrincipal();
                permissionsSuperset = authenticatedUser.getPermissionsSuperset();
            }
        } catch (AuthenticationException e) {
            throw new AuthException(e, AuthenticationError.INVALID_USER_CREDENTIALS);
        }

        userService.updateLastLogin(user.getId(), Instant.now());

        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        return generateAuthToken(user, permissionsSuperset, expirationInSecs);
    }

    private AuthenticationManager getAuthenticationManager(User user) {
        return user.getSource() == Source.LDAP
                ? ldapAuthenticationManager
                : authenticationManager;
    }

    @Override
    @Transactional
    public AuthenticationData refresh(String refreshToken) {
        RefreshTokenContent refreshTokenContent = jwtService.parseRefreshToken(refreshToken);
        if (refreshTokenContent.getExpiration().isBefore(Instant.now())) {
            throw new AuthException(AuthenticationError.REFRESH_TOKEN_IS_EXPIRED);
        }

        // there are two types of tokens: authentication token and access one.
        // the access token is used by test reporting clients. it is supposed that these clients should use only
        // the apis that does not require any explicit permission assigned (but it can require user to be authenticated)
        int refreshTokenExpiration = tokenProperties.getAuth().getExpirationInSecs().getRefresh();
        Instant authTokenMaxExpiration = Instant.now().plusSeconds(refreshTokenExpiration);
        return authTokenMaxExpiration.isAfter(refreshTokenContent.getExpiration())
                ? refreshAuthToken(refreshTokenContent)
                : refreshAccessToken(refreshTokenContent);
    }

    private AuthenticationData refreshAuthToken(RefreshTokenContent refreshTokenContent) {
        Integer userId = refreshTokenContent.getUserId();
        User user = userService.getWithPermissionsById(userId)
                               .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID, userId));
        if (UserStatus.INACTIVE.equals(user.getStatus())) {
            throw new ForbiddenOperationException(ForbiddenOperationError.USER_NOT_ACTIVE, userId);
        }
        if (!Objects.equals(user.getPassword(), refreshTokenContent.getPassword())) {
            throw new AuthException(AuthenticationError.USER_PASSWORD_HAS_CHANGED);
        }

        userService.updateLastLogin(user.getId(), Instant.now());

        Set<String> permissionsSuperset = permissionService.getSuperset(user.getGroups(), user.getPermissions());
        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        return generateAuthToken(user, permissionsSuperset, expirationInSecs);
    }

    private AuthenticationData refreshAccessToken(RefreshTokenContent refreshTokenContent) {
        Integer userId = refreshTokenContent.getUserId();
        User user = userService.getById(userId)
                               .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID, userId));
        if (UserStatus.INACTIVE.equals(user.getStatus())) {
            throw new ForbiddenOperationException(ForbiddenOperationError.USER_NOT_ACTIVE, userId);
        }
        if (!Objects.equals(user.getPassword(), refreshTokenContent.getPassword())) {
            throw new AuthException(AuthenticationError.USER_PASSWORD_HAS_CHANGED);
        }

        int expirationInSecs = tokenProperties.getAuth().getExpirationInSecs().getReportingClient();
        return generateAuthToken(user, Set.of(), expirationInSecs);
    }

    @Override
    public String generateServiceAccessToken(Integer userId) {
        User user = userService.getById(userId)
                               .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID, userId));
        return jwtService.generateAccessToken(userId, user.getPassword());
    }

    @Override
    public boolean verify(String authToken, Set<String> permissions) {
        AuthTokenContent tokenContent = jwtService.parseAuthToken(authToken);
        return tokenContent.getPermissions() != null
                && tokenContent.getPermissions().containsAll(permissions);
    }

    private AuthenticationData generateReportingClientToken(User user, Set<String> permissionsSuperset) {
        int reportingClient = tokenProperties.getAuth().getExpirationInSecs().getReportingClient();
        return generateAuthToken(user, permissionsSuperset, reportingClient);
    }

    private AuthenticationData generateAuthToken(User user, Set<String> permissionsSuperset) {
        int auth = tokenProperties.getAuth().getExpirationInSecs().getAuthentication();
        return generateAuthToken(user, permissionsSuperset, auth);
    }

    private AuthenticationData generateAuthToken(User user, Set<String> permissionsSuperset, Integer expirationInSecs) {
        Integer userId = user.getId();
        String username = user.getUsername();

        String authToken = jwtService.generateAuthToken(userId, username, permissionsSuperset, expirationInSecs);
        String refreshToken = jwtService.generateRefreshToken(userId, user.getPassword());

        return AuthenticationData.builder()
                                 .userId(userId)
                                 .userSource(user.getSource())
                                 .permissionsSuperset(permissionsSuperset)
                                 .previousLogin(user.getLastLogin())
                                 .authTokenType("Bearer")
                                 .authToken(authToken)
                                 .authTokenExpirationInSecs(expirationInSecs)
                                 .refreshToken(refreshToken)
                                 .build();
    }

}
