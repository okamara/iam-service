package com.zebrunner.iam.service.impl;

import com.zebrunner.iam.domain.token.AuthTokenContent;
import com.zebrunner.iam.domain.token.RefreshTokenContent;
import com.zebrunner.iam.service.JwtService;
import com.zebrunner.iam.service.config.TokenProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Component
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService {

    public static final String AUTHENTICATION_TOKEN_CLAIM_USERNAME = "username";
    public static final String AUTHENTICATION_TOKEN_CLAIM_PERMISSIONS = "permissions";
    public static final String REFRESH_TOKEN_CLAIM_PASSWORD = "password";

    private final TokenProperties tokenProperties;

    @Override
    public String generateAuthToken(Integer userId, String username, Set<String> permissions, int expirationInSecs) {
        Claims claims = Jwts.claims().setSubject(userId.toString());
        claims.put(AUTHENTICATION_TOKEN_CLAIM_USERNAME, username);
        claims.put(AUTHENTICATION_TOKEN_CLAIM_PERMISSIONS, permissions);
        return buildToken(claims, expirationInSecs);
    }

    @Override
    public String generateRefreshToken(Integer userId, String password) {
        Claims claims = Jwts.claims().setSubject(userId.toString());
        claims.put(REFRESH_TOKEN_CLAIM_PASSWORD, password);
        return buildToken(claims, tokenProperties.getAuth().getExpirationInSecs().getRefresh());
    }

    @Override
    public String generateAccessToken(Integer userId, String password) {
        Claims claims = Jwts.claims().setSubject(userId.toString());
        claims.put(REFRESH_TOKEN_CLAIM_PASSWORD, password);
        return buildToken(claims, tokenProperties.getAuth().getExpirationInSecs().getAccess());
    }

    private String buildToken(Claims claims, int exp) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, exp);
        return Jwts.builder()
                   .setClaims(claims)
                   .signWith(SignatureAlgorithm.HS512, tokenProperties.getAuth().getSecret())
                   .setExpiration(calendar.getTime())
                   .compact();
    }

    @Override
    public AuthTokenContent parseAuthToken(String token) {
        Claims jwtBody = getTokenBody(token);
        Integer userId = Integer.valueOf(jwtBody.getSubject());
        String username = jwtBody.get(AUTHENTICATION_TOKEN_CLAIM_USERNAME, String.class);
        List<String> permissions = (List<String>) jwtBody.get(AUTHENTICATION_TOKEN_CLAIM_PERMISSIONS, List.class);
        return new AuthTokenContent(userId, username, new HashSet<>(permissions));
    }

    @Override
    public RefreshTokenContent parseRefreshToken(String token) {
        Claims body = getTokenBody(token);
        return new RefreshTokenContent(
                Integer.valueOf(body.getSubject()),
                body.get(REFRESH_TOKEN_CLAIM_PASSWORD, String.class),
                body.getExpiration().toInstant()
        );
    }

    private Claims getTokenBody(String token) {
        String secret = tokenProperties.getAuth().getSecret();
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

}