package com.zebrunner.iam.service;

import com.zebrunner.iam.domain.token.AuthTokenContent;
import com.zebrunner.iam.domain.token.RefreshTokenContent;

import java.util.Set;

public interface JwtService {

    String generateAuthToken(Integer userId, String username, Set<String> permissions, int expirationInSecs);

    String generateRefreshToken(Integer userId, String password);

    String generateAccessToken(Integer userId, String password);

    AuthTokenContent parseAuthToken(String token);

    RefreshTokenContent parseRefreshToken(String token);

}
