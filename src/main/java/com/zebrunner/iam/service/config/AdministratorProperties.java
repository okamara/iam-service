package com.zebrunner.iam.service.config;

import com.zebrunner.iam.service.exception.ServiceInitializationException;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.annotation.PostConstruct;

@Data
@ConfigurationProperties(prefix = "service.administrator")
public class AdministratorProperties {

    private String username;
    private String password;
    private String group;

    @PostConstruct
    private void init() {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(password) || StringUtils.isBlank(group)) {
            throw new ServiceInitializationException("Information about default administrator is not provided.");
        }
    }

}
