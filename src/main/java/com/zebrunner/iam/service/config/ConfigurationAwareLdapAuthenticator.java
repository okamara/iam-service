package com.zebrunner.iam.service.config;

import com.zebrunner.iam.domain.entity.LdapConfiguration;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.service.LdapService;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import lombok.RequiredArgsConstructor;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.core.Authentication;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticator;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ConfigurationAwareLdapAuthenticator implements LdapAuthenticator {

    private final LdapService ldapService;

    @Override
    public DirContextOperations authenticate(Authentication authentication) {
        return ldapAuthenticator().authenticate(authentication);
    }

    private LdapAuthenticator ldapAuthenticator() {
        LdapConfiguration ldapConfiguration = ldapService.getConfiguration()
                                                         .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.LDAP_CONFIGURATION_NOT_FOUND));
        LdapContextSource ldapContextSource = getLdapContextSource(ldapConfiguration);

        FilterBasedLdapUserSearch filterBasedLdapUserSearch = new FilterBasedLdapUserSearch(
                ldapConfiguration.getDn(), ldapConfiguration.getSearchFilter(), ldapContextSource
        );
        filterBasedLdapUserSearch.setSearchSubtree(true);

        BindAuthenticator bindAuthenticator = new BindAuthenticator(ldapContextSource);
        bindAuthenticator.setUserSearch(filterBasedLdapUserSearch);
        bindAuthenticator.afterPropertiesSet();

        return bindAuthenticator;
    }

    private LdapContextSource getLdapContextSource(LdapConfiguration ldapConfiguration) {
        LdapContextSource ldapContextSource = new LdapContextSource();

        ldapContextSource.setUrl(ldapConfiguration.getUrl());
        ldapContextSource.setUserDn(ldapConfiguration.getManagerUser());
        ldapContextSource.setPassword(ldapConfiguration.getManagerPassword());
        ldapContextSource.afterPropertiesSet();

        return ldapContextSource;
    }

}
