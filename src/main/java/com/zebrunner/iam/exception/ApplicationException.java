package com.zebrunner.iam.exception;

import lombok.Getter;

@Getter
public abstract class ApplicationException extends RuntimeException {

    private final ErrorDetail errorDetail;
    private Object[] messageArgs;

    protected ApplicationException(ErrorDetail errorDetail) {
        this.errorDetail = errorDetail;
    }

    protected ApplicationException(Throwable cause, ErrorDetail errorDetail) {
        super(cause);
        this.errorDetail = errorDetail;
    }

    protected ApplicationException(ErrorDetail errorDetail, Object... messageArgs) {
        this(errorDetail);
        this.messageArgs = messageArgs;
    }

    protected ApplicationException(Throwable cause, ErrorDetail errorDetail, Object... messageArgs) {
        this(cause, errorDetail);
        this.messageArgs = messageArgs;
    }

    @Override
    public String getMessage() {
        return errorDetail.getCode() + " - " + errorDetail.toString();
    }

}
