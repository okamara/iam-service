package com.zebrunner.iam.exception;

/**
 * Exception that is meant to be thrown when we get a technical runtime error from integrated system
 * (e.g. connection timeout).
 */
public class ExternalSystemException extends ApplicationException {

    public ExternalSystemException(ErrorDetail errorDetail, String message) {
        super(errorDetail, message);
    }

}
