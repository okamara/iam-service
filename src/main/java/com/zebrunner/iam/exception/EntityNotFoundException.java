package com.zebrunner.iam.exception;

/**
 * Exception that should be thrown to indicate that requested entity is not found (because of the actual absence of
 * such entity or lack of permissions required to retrieve it).
 */
public class EntityNotFoundException extends ApplicationException {

    public EntityNotFoundException(ErrorDetail errorDetail, Object... messageArgs) {
        super(errorDetail, messageArgs);
    }

}
