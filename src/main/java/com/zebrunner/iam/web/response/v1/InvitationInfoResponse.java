package com.zebrunner.iam.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.googlecode.jmapper.annotations.JMapConversion;
import com.zebrunner.iam.domain.entity.Group;
import com.zebrunner.iam.domain.entity.Source;
import com.zebrunner.iam.domain.entity.User;
import lombok.Data;
import lombok.Value;

import java.time.Instant;

@Data
@JGlobalMap
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InvitationInfoResponse {

    private Integer id;
    private String email;
    private String token;
    private Invitor invitor;
    private GroupInfo group;
    private Source source;
    private Instant createdAt;
    private Instant modifiedAt;

    @Value
    public static class Invitor {

        Integer id;
        String username;

    }

    @Value
    public static class GroupInfo {

        Integer id;
        String name;

    }

    @JMapConversion(from = {"invitor"}, to = {"invitor"})
    public Invitor convertInvitor(User user) {
        return new Invitor(user.getId(), user.getUsername());
    }

    @JMapConversion(from = {"group"}, to = {"group"})
    public GroupInfo convertGroup(Group group) {
        return new GroupInfo(group.getId(), group.getName());
    }

}
