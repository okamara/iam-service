package com.zebrunner.iam.web.response.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.googlecode.jmapper.annotations.JGlobalMap;
import com.zebrunner.iam.domain.entity.Source;
import lombok.Data;

import java.util.Set;

@Data
@JGlobalMap
@JsonInclude(Include.NON_NULL)
public class AuthenticationDataResponse {

    private Integer userId;
    private Source userSource;
    private Set<String> permissionsSuperset;

    private String authTokenType;
    private String authToken;
    private int authTokenExpirationInSecs;

    private String refreshToken;

}
