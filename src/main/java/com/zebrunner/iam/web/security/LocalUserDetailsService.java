package com.zebrunner.iam.web.security;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
public class LocalUserDetailsService implements UserDetailsService {

    private final UserService userService;
    private final PermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userService.getWithPermissionsByUsername(username)
                               .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " was not found"));
        Set<String> permissions = permissionService.getSuperset(user.getGroups(), user.getPermissions());
        return new AuthenticatedUser(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getStatus(),
                permissions
        );
    }

}
