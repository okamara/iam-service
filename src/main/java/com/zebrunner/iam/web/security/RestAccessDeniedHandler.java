package com.zebrunner.iam.web.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zebrunner.iam.exception.CommonErrorDetails;
import com.zebrunner.iam.web.error.ErrorResponse;
import com.zebrunner.iam.web.error.ErrorResponseHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

/**
 * The RestAccessDeniedHandler is called by the ExceptionTranslationFilter to handle all AccessDeniedExceptions. These
 * exceptions are thrown when the authentication is valid but access is not authorized.
 */
@Component
@RequiredArgsConstructor
public class RestAccessDeniedHandler implements AccessDeniedHandler {

    private final ErrorResponseHelper errorResponseHelper;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
            throws IOException {
        ErrorResponse errorResponse = errorResponseHelper.buildFromDetail(CommonErrorDetails.ACCESS_IS_DENIED);
        ObjectMapper objMapper = new ObjectMapper();

        final HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(response);
        wrapper.setStatus(HttpStatus.FORBIDDEN.value());
        wrapper.setContentType(MediaType.APPLICATION_JSON_VALUE);
        wrapper.getWriter().println(objMapper.writeValueAsString(errorResponse));
        wrapper.getWriter().flush();
    }
}
