package com.zebrunner.iam.web.request.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.googlecode.jmapper.annotations.JGlobalMap;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@JGlobalMap
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateInvitedUserRequest {

    @NotEmpty(message = "Username required")
    @Pattern(regexp = "[\\w-]+", message = "Invalid format")
    private String username;

    private String password;

    @Email
    @NotEmpty
    private String email;

    @Size(min = 1, max = 100, message = "“First name should be 1 to 100 characters long")
    @Pattern(regexp = "^[A-Za-z0-9-.]+$", message = "First name should contain only letters, numbers, dashes and dots")
    private String firstName;

    @Size(min = 1, max = 100, message = "Last name should be 1 to 100 characters long")
    @Pattern(regexp = "^[A-Za-z0-9-.]+$", message = "Last name should contain only letters, numbers, dashes and dots")
    private String lastName;

    private String photoUrl;

}
