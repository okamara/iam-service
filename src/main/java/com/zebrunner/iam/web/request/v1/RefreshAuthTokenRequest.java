package com.zebrunner.iam.web.request.v1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RefreshAuthTokenRequest {

    @NotBlank
    private String refreshToken;

}
