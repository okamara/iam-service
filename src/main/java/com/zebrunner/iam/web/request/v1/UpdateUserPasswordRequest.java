package com.zebrunner.iam.web.request.v1;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UpdateUserPasswordRequest {

    private String oldPassword;
    @NotEmpty
    private String newPassword;

}
