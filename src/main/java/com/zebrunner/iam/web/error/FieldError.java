package com.zebrunner.iam.web.error;

import lombok.Value;

@Value
public class FieldError {

    String field;
    String message;

}
