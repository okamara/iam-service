package com.zebrunner.iam.web.error;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@JsonInclude(Include.NON_EMPTY)
public class ErrorResponse {

    private final String code;
    private final String message;
    private List<FieldError> fieldErrors;

}
