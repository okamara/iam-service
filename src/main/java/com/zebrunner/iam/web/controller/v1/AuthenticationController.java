package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.iam.domain.token.AuthenticationData;
import com.zebrunner.iam.service.AuthenticationService;
import com.zebrunner.iam.web.documentation.v1.AuthenticationDocumentedController;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.request.v1.LoginRequest;
import com.zebrunner.iam.web.request.v1.VerifyPermissionsRequest;
import com.zebrunner.iam.web.request.v1.RefreshAuthTokenRequest;
import com.zebrunner.iam.web.response.v1.AuthenticationDataResponse;
import com.zebrunner.iam.web.security.AuthenticatedUser;
import com.zebrunner.iam.web.security.Principal;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/auth", produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthenticationController implements AuthenticationDocumentedController {

    private static final String FIRST_LOGIN_HEADER = "x-zbr-first-login";

    private final Mapper mapper;
    private final AuthenticationService authenticationService;

    @Override
    @PostMapping("/login")
    public ResponseEntity<AuthenticationDataResponse> login(@RequestBody @Valid LoginRequest loginRequest) {
        AuthenticationData authenticationData = authenticationService
                .authenticate(loginRequest.getUsername(), loginRequest.getPassword());

        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        builder.header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, FIRST_LOGIN_HEADER)
               .header(FIRST_LOGIN_HEADER, Boolean.toString(authenticationData.getPreviousLogin() == null));

        return builder.body(mapper.map(authenticationData, AuthenticationDataResponse.class));
    }

    @Override
    @PostMapping("/refresh")
    public AuthenticationDataResponse refresh(@RequestBody RefreshAuthTokenRequest refreshAuthTokenRequest) {
        String refreshToken = refreshAuthTokenRequest.getRefreshToken();
        AuthenticationData authenticationData = authenticationService.refresh(refreshToken);
        return mapper.map(authenticationData, AuthenticationDataResponse.class);
    }

    @Override
    @GetMapping("/access")
    public Map<String, String> getServiceRefreshToken(@Principal AuthenticatedUser authenticatedUser) {
        String serviceAccessToken = authenticationService.generateServiceAccessToken(authenticatedUser.getUserId());
        return Map.of("token", serviceAccessToken);
    }

    @Override
    @PostMapping("/verification")
    public ResponseEntity<Void> checkPermissions(@RequestBody @Valid VerifyPermissionsRequest verifyPermissionsRequest) {
        String authToken = verifyPermissionsRequest.getAuthToken();
        Set<String> permissions = verifyPermissionsRequest.getPermissions();

        boolean result = authenticationService.verify(authToken, permissions);
        return new ResponseEntity<>(result ? HttpStatus.OK : HttpStatus.FORBIDDEN);
    }

}
