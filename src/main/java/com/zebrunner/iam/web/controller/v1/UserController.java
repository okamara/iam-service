package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.SortOrder;
import com.zebrunner.iam.domain.search.UserSearchCriteria;
import com.zebrunner.iam.exception.EntityNotFoundException;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.service.exception.EntityNotFoundError;
import com.zebrunner.iam.web.documentation.v1.UserDocumentedController;
import com.zebrunner.iam.web.response.v1.FullUserInfoResponse;
import com.zebrunner.iam.web.mapper.Mapper;
import com.zebrunner.iam.web.patch.PatchOperation;
import com.zebrunner.iam.web.patch.PatchRequest;
import com.zebrunner.iam.web.patch.ValidPatch;
import com.zebrunner.iam.web.request.v1.CreateInvitedUserRequest;
import com.zebrunner.iam.web.request.v1.CreateUserRequest;
import com.zebrunner.iam.web.request.v1.UpdateUserPasswordRequest;
import com.zebrunner.iam.web.security.HasPermission;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController implements UserDocumentedController {

    private final Mapper mapper;
    private final UserService userService;

    @Override
    @GetMapping
    @PreAuthorize("hasAnyPermission('VIEW_USERS', 'MODIFY_USERS') or #isPublic")
    public SearchResult<FullUserInfoResponse> search(@RequestParam(name = "query", required = false) String query,
                                                     @RequestParam(name = "sortBy", required = false) String sortBy,
                                                     @RequestParam(name = "sortOrder", required = false) SortOrder sortOrder,
                                                     @RequestParam(name = "page", defaultValue = "1") Integer page,
                                                     @RequestParam(name = "pageSize", defaultValue = "20") Integer pageSize,
                                                     @RequestParam(name = "public", defaultValue = "true") boolean isPublic,
                                                     @RequestParam(name = "status", required = false) UserStatus status) {
        UserSearchCriteria searchCriteria = new UserSearchCriteria(
                query, sortBy, sortOrder, page, pageSize, isPublic, status
        );
        SearchResult<User> searchResult = userService.search(searchCriteria);
        return new SearchResult<>(searchResult, mapper.map(searchResult.getResults(), FullUserInfoResponse.class));
    }

    @Override
    @GetMapping(params = "username")
    public FullUserInfoResponse getProfile(@RequestParam("username") @NotEmpty String username) {
        return userService.getWithPermissionsByUsername(username)
                          .map(user -> mapper.map(user, FullUserInfoResponse.class))
                          .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_USERNAME));
    }

    @Override
    @GetMapping("/{id}")
    public FullUserInfoResponse getProfile(@PathVariable("id") @Positive Integer id) {
        return userService.getWithPermissionsById(id)
                          .map(user -> mapper.map(user, FullUserInfoResponse.class))
                          .orElseThrow(() -> new EntityNotFoundException(EntityNotFoundError.USER_NOT_FOUND_BY_ID));
    }

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasPermission('MODIFY_USERS')")
    public CreateUserRequest create(@RequestBody @Valid CreateUserRequest createUserRequest) {
        User user = mapper.map(createUserRequest, User.class);
        user = userService.create(user);
        return mapper.map(user, CreateUserRequest.class);
    }

    @Override
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(params = "invitation-token")
    public CreateUserRequest create(@RequestParam("invitation-token") @NotEmpty String invitationToken,
                                    @RequestBody @Valid CreateInvitedUserRequest createInvitedUserRequest) {
        User user = mapper.map(createInvitedUserRequest, User.class);
        user = userService.create(invitationToken, user);
        return mapper.map(user, CreateUserRequest.class);
    }

    @Override
    @PostMapping("/{id}/password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updatePassword(@PathVariable("id") @Positive Integer userId,
                               @RequestBody @Valid UpdateUserPasswordRequest updaterequestUserPassword,
                               @HasPermission("MODIFY_USERS") boolean forceUpdate) {
        String oldPassword = updaterequestUserPassword.getOldPassword();
        String newPassword = updaterequestUserPassword.getNewPassword();
        userService.updatePassword(userId, oldPassword, newPassword, forceUpdate);
    }

    @Override
    @PatchMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void patch(@PathVariable("id") Integer id,
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/email")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/firstName")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/lastName")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/photoUrl")
                      @ValidPatch(op = PatchOperation.REPLACE, path = "/status")
                      @RequestBody @Valid PatchRequest patchRequest,
                      @HasPermission("MODIFY_USERS") boolean fullUpdate) {
        User.UserBuilder userBuilder = User.builder()
                                           .id(id)
                                           .firstName(patchRequest.getReplaceValue("/firstName", String.class))
                                           .lastName(patchRequest.getReplaceValue("/lastName", String.class))
                                           .photoUrl(patchRequest.getReplaceValue("/photoUrl", String.class));

        if (fullUpdate) {
            userBuilder.email(patchRequest.getReplaceValue("/email", String.class))
                       .status(patchRequest.getReplaceValue("/status", UserStatus.class));
        }

        userService.patch(userBuilder.build());
    }

    @Override
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping("/{userId}/groups/{groupId}")
    @PreAuthorize("hasPermission('MODIFY_USER_GROUPS')")
    public void addUserToGroup(@PathVariable("userId") @Positive Integer userId,
                               @PathVariable("groupId") @Positive Integer groupId) {
        userService.addUserToGroup(userId, groupId);
    }

    @Override
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{userId}/groups/{groupId}")
    @PreAuthorize("hasPermission('MODIFY_USER_GROUPS')")
    public void removeUserFromGroup(@PathVariable("userId") @Positive Integer userId,
                                    @PathVariable("groupId") @Positive Integer groupId) {
        userService.removeUserFromGroup(userId, groupId);
    }

}
