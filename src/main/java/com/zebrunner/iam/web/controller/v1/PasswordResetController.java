package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.iam.domain.entity.User;
import com.zebrunner.iam.service.UserService;
import com.zebrunner.iam.web.documentation.v1.PasswordResetDocumentedController;
import com.zebrunner.iam.web.request.v1.ResetPasswordRequest;
import com.zebrunner.iam.web.request.v1.SendPasswordResetRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.Optional;

@Validated
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/users/password-resets", produces = MediaType.APPLICATION_JSON_VALUE)
public class PasswordResetController implements PasswordResetDocumentedController {

    private final UserService userService;

    @Override
    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void sendResetPasswordEmail(@RequestBody @Valid SendPasswordResetRequest sendPasswordResetRequest) {
        userService.sendResetPasswordEmail(sendPasswordResetRequest.getEmail());
    }

    @Override
    @GetMapping(params = "token")
    public ResponseEntity<Void> checkIfTokenResetIsPossible(@RequestParam("token") @NotEmpty String token) {
        Optional<User> maybeUser = userService.getByResetToken(token);
        return new ResponseEntity<>(maybeUser.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @Override
    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void resetPassword(@RequestBody @Valid ResetPasswordRequest resetPasswordRequest) {
        userService.updatePassword(resetPasswordRequest.getResetToken(), resetPasswordRequest.getNewPassword());
    }

}
