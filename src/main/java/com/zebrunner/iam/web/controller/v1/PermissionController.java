package com.zebrunner.iam.web.controller.v1;

import com.zebrunner.iam.domain.entity.Permission;
import com.zebrunner.iam.service.PermissionService;
import com.zebrunner.iam.web.documentation.v1.PermissionDocumentedController;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/v1/permissions", produces = MediaType.APPLICATION_JSON_VALUE)
public class PermissionController implements PermissionDocumentedController {

    private final PermissionService permissionService;

    @Override
    @GetMapping
    @PreAuthorize("hasPermission('MODIFY_USER_GROUPS')")
    public List<String> getAll() {
        return permissionService.getAll().stream()
                                .map(Permission::getName)
                                .collect(Collectors.toList());
    }

}
