package com.zebrunner.iam.web.patch;

import com.zebrunner.iam.exception.BusinessValidationException;
import com.zebrunner.iam.service.exception.BusinessValidationError;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

@Aspect
@Component
public class PatchValidationAspect {

    private final Map<Class<? extends Annotation>, Function<Annotation, ValidPatch[]>> validAnnotations = Map.of(
            ValidPatch.class, annotation -> new ValidPatch[]{(ValidPatch) annotation},
            ValidPatch.List.class, annotation -> ((ValidPatch.List) annotation).value()
    );

    @Before("execution(* *(.., @com.zebrunner.iam.web.patch.ValidPatch (*) , ..)) || " +
            "execution(* *(.., @com.zebrunner.iam.web.patch.ValidPatch.List (*) , ..))")
    public void validatePatchRequest(JoinPoint joinPoint) throws NoSuchMethodException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        String methodName = signature.getMethod().getName();
        Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();

        Annotation[][] annotations = joinPoint.getTarget()
                                              .getClass()
                                              .getMethod(methodName, parameterTypes)
                                              .getParameterAnnotations();

        for (int i = 0; i < annotations.length; i++) {
            for (int j = 0; j < annotations[i].length; j++) {

                Annotation annotation = annotations[i][j];
                Function<Annotation, ValidPatch[]> mapper = validAnnotations.get(annotation.annotationType());

                if (mapper != null) {
                    if (joinPoint.getArgs()[i] instanceof PatchRequest) {
                        validate((PatchRequest) joinPoint.getArgs()[i], mapper.apply(annotation));
                    } else {
                        throw new RuntimeException("@ValidPatch should only be applied on PatchRequest instance");
                    }
                }
            }
        }
    }

    private void validate(PatchRequest patchRequest, ValidPatch... annotations) {
        for (PatchRequest.Item item : patchRequest) {
            String path = item.getPath();
            Stream.of(annotations)
                  .filter(validPatch -> validPatch.op() == item.getOperation())
                  .filter(validPatch -> validPatch.path().equals(path))
                  .findFirst()
                  .orElseThrow(() -> new BusinessValidationException(
                          BusinessValidationError.PATCH_REQUEST_OPERATION_NOT_SUPPORTED_FOR_PATH,
                          item.getOperation().name().toLowerCase(),
                          path
                  ));
        }
    }

}
