package com.zebrunner.iam.web.documentation.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import java.util.List;

public interface PermissionDocumentedController {

    @Operation(
            summary = "Retrieves all the existing permissions.",
            description = "Retrieves all the existing permissions as a list of strings. No paging/filtering/ordering supported.",
            tags = "Permissions",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            content = @Content(
                                    array = @ArraySchema(minItems = 0, schema = @Schema(implementation = String.class))
                            ),
                            description = "If there were no errors."
                    ),
            }
    )
    List<String> getAll();

}
