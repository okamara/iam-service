package com.zebrunner.iam.web.documentation.v1;

import com.zebrunner.iam.domain.entity.UserStatus;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.SortOrder;
import com.zebrunner.iam.web.request.v1.UpdateUserPasswordRequest;
import com.zebrunner.iam.web.response.v1.FullUserInfoResponse;
import com.zebrunner.iam.web.patch.PatchRequest;
import com.zebrunner.iam.web.request.v1.CreateInvitedUserRequest;
import com.zebrunner.iam.web.request.v1.CreateUserRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

public interface UserDocumentedController {

    @Operation(
            tags = "Users"
    )
    SearchResult<FullUserInfoResponse> search(String query, String sortBy, SortOrder sortOrder, Integer page, Integer pageSize, boolean isPublic, UserStatus status);

    @Operation(
            summary = "Retrieves a user profile by username.",
            description = "Retrieves a user profile by username.",
            tags = "Users",
            parameters = @Parameter(name = "username", description = "The username."),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Returns the user profile.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = FullUserInfoResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Validation error.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the user does not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    FullUserInfoResponse getProfile(String username);

    @Operation(
            summary = "Retrieves a user profile by user id.",
            description = "Retrieves a user profile by user id.",
            tags = "Users",
            parameters = @Parameter(name = "id", description = "The user id."),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Returns the user profile.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = FullUserInfoResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Validation error.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the user does not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    FullUserInfoResponse getProfile(Integer id);

    @Operation(
            summary = "Creates a user.",
            description = "Returns the created user.",
            tags = "Users",
            requestBody = @RequestBody(
                    description = "The user to create.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = CreateUserRequest.class)
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Returns the created user profile.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = CreateUserRequest.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Validation error.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the invitation token is not valid.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    CreateUserRequest create(CreateUserRequest createUserRequest);

    @Operation(
            summary = "Creates user using provided invitation token.",
            description = "Creates user using provided invitation token.",
            tags = "Users",
            parameters = @Parameter(name = "invitation-token", description = "The invitation-token to create a user.", required = false),
            requestBody = @RequestBody(
                    description = "Short information about new user.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = CreateUserRequest.class)
                    )
            ),
            responses = {
                    @ApiResponse(responseCode = "201", description = "Returns the created user profile."),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Validation error.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the invitation token is not valid.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    CreateUserRequest create(String invitationToken, CreateInvitedUserRequest createInvitedUserRequest);

    @Operation(
            summary = "Updates a user password.",
            description = "Only the profile owner and admin have access to update user password.",
            tags = "Users",
            parameters = @Parameter(name = "userId", description = "The user id."),
            requestBody = @RequestBody(
                    description = "A json-patch request containing information about changed user info.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = UpdateUserPasswordRequest.class)
                    )
            ),
            responses = {
                    @ApiResponse(responseCode = "204", description = "User password was updated successfully."),
                    @ApiResponse(
                            responseCode = "401",
                            description = "Indicates that it’s not the profile owner or admin who tries to update the password.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the user does not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    void updatePassword(Integer userId, UpdateUserPasswordRequest updaterequestUserPassword, boolean forceUpdate);

    @Operation(
            summary = "Patches a user info.",
            description = "Can be used to change user status, email, firstName, lastName or photoUrl depending on principal's permissions.",
            tags = "Users",
            parameters = @Parameter(name = "userId", description = "The user id."),
            requestBody = @RequestBody(
                    description = "A json-patch request containing information about changed user info.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(
                                    schema = @Schema(implementation = PatchRequest.Item.class),
                                    arraySchema = @Schema(implementation = PatchRequest.class)
                            ),
                            examples = @ExampleObject("[{\"op\": \"replace\", \"path\": \"/firstName\", \"value\": \"Niki\"}]")
                    )
            ),
            responses = {
                    @ApiResponse(responseCode = "204", description = "The user was updated successfully."),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Indicates that the user email has already been taken be another user.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the user does not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    void patch(Integer id, PatchRequest patchRequest, boolean fullUpdate);

    @Operation(
            summary = "Adds a user to a group.",
            description = "Adds a user to a group by its id.",
            tags = "User-Groups",
            parameters = {
                    @Parameter(name = "userId", description = "The user id."),
                    @Parameter(name = "groupId", description = "The group id.")
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "The user was added successfully."),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Indicates that the user has already been added to this group.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the group or user do not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    void addUserToGroup(Integer userId, Integer groupId);

    @Operation(
            summary = "Deletes a user from a group.",
            description = "Deletes a user from a group.",
            tags = "User-Groups",
            parameters = {
                    @Parameter(name = "userId", description = "The user id."),
                    @Parameter(name = "groupId", description = "The group id.")
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "The user was detached successfully."),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Indicates that the user has not been added to this group.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the group or user do not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    void removeUserFromGroup(Integer userId, Integer groupId);

}
