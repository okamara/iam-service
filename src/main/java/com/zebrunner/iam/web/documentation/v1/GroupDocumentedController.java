package com.zebrunner.iam.web.documentation.v1;

import com.zebrunner.iam.domain.search.SearchCriteria;
import com.zebrunner.iam.domain.search.SearchResult;
import com.zebrunner.iam.domain.search.SortOrder;
import com.zebrunner.iam.web.request.v1.SaveGroupRequest;
import com.zebrunner.iam.web.response.v1.GroupInfoResponse;
import com.zebrunner.iam.web.response.v1.FullGroupInfoResponse;
import com.zebrunner.iam.web.patch.PatchRequest;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.Explode;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.enums.ParameterStyle;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

public interface GroupDocumentedController {

    @Operation(
            summary = "Retrieves a paginated set of groups.",
            description = "Returns found groups",
            tags = "Groups",
            parameters = {
                    @Parameter(
                            name = "searchCriteria",
                            in = ParameterIn.QUERY,
                            explode = Explode.TRUE,
                            style = ParameterStyle.DEEPOBJECT,
                            schema = @Schema(implementation = SearchCriteria.class)
                    )
            },
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = SearchResult.class)
                            ),
                            description = "Returns found groups"
                    )
            }
    )
    SearchResult<FullGroupInfoResponse> search(String query, String sortBy, SortOrder sortOrder, Integer page, Integer pageSize, boolean isPublic);

//    @Operation(
//            summary = "Retrieves a default group.",
//            description = "Returns the found default group.",
//            tags = "Groups",
//            responses = {
//                    @ApiResponse(
//                            responseCode = "200",
//                            description = "Returns the found default group.",
//                            content = @Content(
//                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
//                                    schema = @Schema(implementation = GroupDto.class)
//                            )
//                    ),
//                    @ApiResponse(
//                            responseCode = "404",
//                            description = "Indicates that there is no a default group.",
//                            content = @Content(
//                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
//                                    schema = @Schema
//                            )
//                    )
//            }
//    )
//    GroupDto getDefault();

    @Operation(
            summary = "Retrieves a group by its id.",
            description = "Returns the found group.",
            tags = "Groups",
            parameters = @Parameter(name = "id", description = "The group id."),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Returns the found group.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = GroupInfoResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the group does not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    GroupInfoResponse getById(Integer id);

    @Operation(
            summary = "Creates a group and grants it specified permissions.",
            description = "Returns the created group with granted permissions",
            tags = "Groups",
            requestBody = @RequestBody(
                    description = "The group to create.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GroupInfoResponse.class)
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "201",
                            description = "Returns the created group with granted permissions.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = GroupInfoResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Indicates that validation error occurred.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    GroupInfoResponse create(SaveGroupRequest saveGroupRequest);

    @Operation(
            summary = "Updates group properties and group permissions.",
            description = "Returns the updated group with granted permissions",
            tags = "Groups",
            parameters = @Parameter(name = "id", description = "Group id to patch."),
            requestBody = @RequestBody(
                    description = "The desired state of the group.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GroupInfoResponse.class)
                    )
            ),
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Returns the updated group with granted permissions.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema(implementation = GroupInfoResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Indicates that the group or any of the desired permission do not exist.",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                                    schema = @Schema
                            )
                    )
            }
    )
    GroupInfoResponse update(Integer id, SaveGroupRequest saveGroupRequest);

    @Operation(
            summary = "Updates isDefault flag for a Group.",
            description = "Switches on and off the isDefault flag for a specific Group. If flag is off and group is not default, nothing happen. If flag is true and the group is not default, makes it a default one.",
            tags = "Groups",
            parameters = @Parameter(name = "id", description = "Group id to patch."),
            requestBody = @RequestBody(
                    description = "A json-patch request containing information about isDefault flag.",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(
                                    schema = @Schema(implementation = PatchRequest.Item.class),
                                    arraySchema = @Schema(implementation = PatchRequest.class)
                            ),
                            examples = @ExampleObject("[{\"op\": \"replace\", \"path\": \"/isDefault\", \"value\": true}]")
                    )
            ),
            responses = {
                    @ApiResponse(responseCode = "204", description = "Indicates that the changes was successfully saved."),
                    @ApiResponse(responseCode = "400", description = "Indicates that the invalid patch request was submitted."),
                    @ApiResponse(responseCode = "404", description = "Indicates that the group does not exist.")
            }
    )
    void setDefault(Integer id, PatchRequest patchRequest);

    @Operation(
            summary = "Deletes a group.",
            description = "Deletes a group by its id and removes all permissions from the group.",
            tags = "Groups",
            responses = {
                    @ApiResponse(responseCode = "204", description = "The group was deleted successfully."),
                    @ApiResponse(responseCode = "400", description = "Indicates that the group contains users. It is required to remove all users before the delete operation."),
                    @ApiResponse(responseCode = "404", description = "Indicates that the group does not exist.")
            }
    )
    void delete(Integer id);

}
