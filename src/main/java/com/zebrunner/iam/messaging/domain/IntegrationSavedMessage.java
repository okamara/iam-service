package com.zebrunner.iam.messaging.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IntegrationSavedMessage {

    private Integer id;
    private String name;
    private boolean enabled;
    private List<Param> params;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Param {

        private Integer id;
        private String name;
        private String value;
        private boolean encrypted;
        private String key;

    }

}
