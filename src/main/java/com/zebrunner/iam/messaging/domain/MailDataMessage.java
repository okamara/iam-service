package com.zebrunner.iam.messaging.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(toBuilder = true)
public final class MailDataMessage {

    private final Set<String> toEmails;
    private final Set<String> ccEmails;
    private final Set<String> bccEmails;
    private final String subject;
    private final String plainText;
    private final String templateName;
    private final Map<String, Object> templateModel;
    private final List<Attachment> attachments;

}
